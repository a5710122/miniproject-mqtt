#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

const char* ssid = "Your Wifi";
const char* password = "";

const char* mqttServer = "Your IP";
const int mqttPort = 1883;
const char* mqttUser = "";
const char* mqttPassword = "";

WiFiClient espClient;
PubSubClient client(espClient);

const int led = 2;

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup() {
  Serial.begin(115200);

  lcd.begin();
  lcd.print("Hello, world!");
  
  pinMode(led, OUTPUT);


  digitalWrite(led, LOW);


  WiFi.begin(ssid, password);


  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");

r
  client.setServer(mqttServer, mqttPort);

 MQTT
  client.setCallback(callback);


  while (!client.connected()) { 
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP8266Client" )) {

      Serial.println("connected");

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);

    }
  }

  client.publish("esp/starter", "Hello from ESP8266");


  client.subscribe("esp/light");

}

void callback(char* topic, byte* payload, unsigned int length) {

  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);


    if (i == 0) {
      if ((char)payload[i] == '0') { //turn off light
        digitalWrite(led, LOW);
        lcd.clear();
        lcd.print("Light Off");
        
        
      } else {
        digitalWrite(led, HIGH);
        lcd.clear();
        lcd.print("Light On");
        
        
      }
    }
  }
  
  Serial.println();
  Serial.println("-----------------------");

}

void loop() {
    
  client.loop();
  

}
